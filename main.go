package main

import (
	"fmt"
	"errors"
)

type Calc struct {
	result int
	// Какое количество операций было сделано
	history []Hit
}

type Hit struct {
	Operation string
	Arg       int
}

func main() {
	c := &Calc{}
	c.Add(5)
	c.Add(8)

	fmt.Printf("%d\n", c.result)
}

func NewCalc(i int) *Calc {
	return &Calc{
		result: i,
	}
}

func (c *Calc) Add(a int) {
	c.result += a
}

func (c *Calc) Mult(i int) {
	c.result *= i
	c.history = append(c.history, Hit{
		Operation: "Mult",
		Arg:       i,
	})
}

func (c *Calc) History() []Hit {
	fmt.Printf("%v", c.history)
	return c.history
}

func (c *Calc) Sub(i int) {
	c.result -= i
	c.history = append(c.history, Hit{
		Operation: "Sub",
		Arg:       i,
	})
}

func (c *Calc) Div(i int) error {
	if i == 0 {
		return errors.New("Divide by zero")
	}
	c.result /= i
	c.history = append(c.history, Hit{
		"Div",
		i,
	})
	return nil
}

func (c *Calc) Result() int {
	return c.result
}

func (c *Calc) Clear() {
	c.result = 0
}
