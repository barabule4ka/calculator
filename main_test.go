package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestCalc_Add_ChangesResult(t *testing.T) {
	c := NewCalc(1)
	c.Add(3)
	assert.Equal(t, 4, c.result)
}

func TestCalc_Mult_ChangeResult(t *testing.T) {
	c := NewCalc(3)
	c.Mult(4)
	assert.Equal(t, 12, c.result)
}

func TestCalc_Mult_ChangeResult1(t *testing.T) {
	c := NewCalc(0)
	c.Mult(3)
	c.Mult(4)
	assert.Equal(t, 0, c.result)
}

func TestCalc_History(t *testing.T) {
	c := NewCalc(0)
	c.Mult(0)
	c.Mult(3)
	c.Mult(4)

	exp := []Hit{
		{
			Operation: "Mult",
			Arg:       0,
		},
		{
			Operation: "Mult",
			Arg:       3,
		},
		{
			Operation: "Mult",
			Arg:       4,
		},
	}

	assert.Equal(t, exp, c.History())
}

func TestCalc_Sub_ChangeResult(t *testing.T) {
	c := NewCalc(8)
	c.Sub(9)

	assert.Equal(t, -1, c.result)
}

func TestCalc_History_Sub(t *testing.T) {
	c := NewCalc(0)
	c.Sub(0)
	c.Sub(56)
	c.Sub(87)

	exp := []Hit{
		{
			"Sub",
			0,
		},
		{
			"Sub",
			56,
		},
		{
			"Sub",
			87,
		},
	}

	assert.Equal(t, exp, c.History())
}

func TestCalc_Div_ByZero(t *testing.T) {
	c := NewCalc(2)
	g := c.Div(0)
	assert.NotNil(t, g)

	assert.Equal(t, 2, c.result)

	assert.EqualError(t, g, "Divide by zero")
}

func TestCalc_Div_ChangeReslt(t *testing.T) {
	c := NewCalc(6)
	g := c.Div(2)
	assert.Nil(t, g)

	assert.Equal(t, 3, c.result)

}

func TestCalc_History_Div(t *testing.T) {
	c := NewCalc(81)
	c.Div(9)
	c.Div(3)
	c.Div(3)

	exp := []Hit{
		{
			"Div",
			9,
		},
		{
			"Div",
			3,
		},
		{
			"Div",
			3,
		},
	}

	assert.Equal(t, exp, c.History())
}

func TestCalc_Clear_EqualZero(t *testing.T) {
	c := NewCalc(7)
	c.Clear()

	assert.Equal(t, 0, c.result)
}

func TestCalc_Result_ReturnResult(t *testing.T) {
	c := NewCalc(9)

	assert.Equal(t, 9, c.Result())
}
